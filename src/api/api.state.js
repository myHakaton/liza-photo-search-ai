import {atom, selector} from "recoil";
import axios from "axios";

export const api = axios.create();

const SelectTagList = selector({
    key: "SelectTagList",
    get: async () => {
        try {
            const response = await api.get("/api/tag/target/all");
            if (response.status === 200) return response.data
            else throw "Ошибка"
        } catch (e) {
            return []
        }
    }
});

export const AtomTagList = atom({
    key: "AtomTagList",
    default: SelectTagList
});

export const AtomByTagList = atom({
    key: "AtomByTagList",
    default: []
});

export const SelectSearchImageByTagList = selector({
    key: "AtomSearchImageByTagList",
    get: async ({get}) => {
        const tagList = get(AtomByTagList);
        if (tagList.length === 0) return [];
        try {
            const response = await api.post("/api/image/by/target/tags", {tags: tagList});
            if (response.status === 200) return response.data
            else throw "Ошибка"
        } catch (e) {
            return []
        }
    }
});