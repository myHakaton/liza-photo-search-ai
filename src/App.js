import React, {Suspense} from 'react';
import './App.css';
import {Layout} from "antd";
import "./App.css";
import {
    BrowserRouter,
    Route, Routes
} from "react-router-dom";
import {HomePage} from "./home/home.component";
import {EditorPage} from "./editor/editor.component";

import {SearchResultPage} from "./search-result/search-result.component";
import {LoadingComponent} from "./loading/loading.component";

function App() {
    return (
        <BrowserRouter>
            <Layout className="App">
                <Suspense fallback={<LoadingComponent/>}>
                    <Routes>
                        <Route path="/" element={<HomePage/>}/>
                        <Route path="/search" element={<SearchResultPage/>}/>
                        <Route path="/editor" element={<EditorPage/>}/>
                    </Routes>
                </Suspense>
                <Layout.Footer className={"footer"}>
                    <img width={46} src={"./logo.png"} alt={""}/>
                    <div>
                        <span className={"text"}>Горячая линия: </span>
                        <span className={"phone"}>8-800-700-54-52</span>
                    </div>
                </Layout.Footer>
            </Layout>
        </BrowserRouter>
    );
}

export default App;
