import {atom, selector} from "recoil";
import {AtomTagList} from "../api/api.state";

export const AtomSearchSelectedTagsList = atom({
    key: "AtomSearchSelectedTagsList",
    default: []
});

export const AtomSearchTagList = selector({
    key: "AtomSearchTagList",
    set: ({set}, value) => {
        set(AtomTagList, value);
    },
    get: ({get}) => {
        return get(AtomTagList);
    }
});
