import {Button, Input, Select, Upload} from "antd";
import {SearchOutlined, CameraOutlined} from "@ant-design/icons"
import {useRecoilState, useRecoilValue, useSetRecoilState} from "recoil";
import {AtomSearchSelectedTagsList, AtomSearchTagList} from "./search.state";
import {useNavigate} from "react-router-dom";
import {api, AtomByTagList} from "../api/api.state";

export const SearchComponent = () => {
    const [value, setValue] = useRecoilState(AtomSearchSelectedTagsList);
    const [tagList, setTagList] = useRecoilState(AtomSearchTagList);
    const setSearchTagList = useSetRecoilState(AtomByTagList);
    const navigate = useNavigate();
    return (
        <Input.Group compact>
            <Select
                mode="tags"
                size={"large"}
                style={{width: 'calc(100% - 80px)'}}
                placeholder="Укажите тег для поиска картинки"
                value={value}
                onChange={setValue}
                maxTagCount={"responsive"}
            >
                {tagList.map((item, index) => (
                    <Select.Option key={index} value={item}>
                        {item}
                    </Select.Option>
                ))}
            </Select>
            <Upload
                fileList={[]}
                customRequest={async ({action, file}) => {
                    const formData = new FormData();
                    formData.append("file", file);
                    await api.post("/api/image/upload", formData);
                    const {data} = await api.post("/api/tag/by/image", formData);
                    const tagListResponse = await api.get("/api/tag/target/all");
                    setTagList([...tagListResponse.data]);
                    setSearchTagList([...data]);
                    setValue([...data]);
                    if (data.length === 0) {
                        navigate("/");
                    } else {
                        navigate("/search");
                    }
                }}
            >
                <Button
                    type={"dashed"}
                    size={"large"}
                    icon={<CameraOutlined/>}
                />
            </Upload>
            <Button
                size={"large"}
                type={"primary"}
                icon={<SearchOutlined/>}
                onClick={() => {
                    setSearchTagList([...value]);
                    if (value.length === 0) {
                        navigate("/");
                    } else {
                        navigate("/search");
                    }
                }}
            />
        </Input.Group>
    )
}