import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import reportWebVitals from './reportWebVitals';
import "antd/dist/antd.variable.min.css";
import './index.css';
import {ConfigProvider} from "antd";
import {RecoilRoot} from "recoil";

ConfigProvider.config({
    theme: {
        primaryColor: "#ed7133"
    }
})

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <RecoilRoot>
        <ConfigProvider>
            <App/>
        </ConfigProvider>
    </RecoilRoot>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
