import {Button, Image, Layout, Modal, Upload} from "antd";
import {SearchComponent} from "../search/search.component";
import React, {useState} from "react";
import "./home.styles.css";
import {DeploymentUnitOutlined, ScanOutlined} from "@ant-design/icons";
import {api} from "../api/api.state";
import {constSelector} from "recoil";
import {useNavigate} from "react-router-dom";

export const HomePage = () => {
    const navigate = useNavigate();
    const [photoUrl, setPhotoUrl] = useState("");
    return (
        <Layout.Content className={"home-content"}>
            <img src={"./logo3.png"} width={180} alt={""}/>
            <div className={"home-search"}>
                <SearchComponent/>
            </div>
            <div className={"actions"}>
                <Button
                    icon={<ScanOutlined/>}
                    onClick={() => {
                        navigate('/editor')
                    }}
                >
                    Анализ фото
                </Button>
                <Upload
                    fileList={[]}
                    customRequest={async ({action, file}) => {
                        const formData = new FormData();
                        formData.append("file", file);
                        const {data} = await api.post("/api/image/uploadWithRec", formData, {responseType: "blob"});
                        const url = window.URL.createObjectURL(data);
                        setPhotoUrl(url);
                    }}
                >
                    <Button icon={<DeploymentUnitOutlined/>}>Распознать фото</Button>
                </Upload>
                <Modal visible={!!photoUrl} closable={false} okButtonProps={{hidden: true}}
                       onCancel={() => setPhotoUrl("")}>
                    <Image src={photoUrl}/>
                </Modal>
            </div>
        </Layout.Content>
    )
};