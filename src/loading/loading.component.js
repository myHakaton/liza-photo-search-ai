import {Layout, Spin} from "antd";
import React from "react";
import "./loading.styles.css";

export const LoadingComponent = () => (
    <React.Fragment>
        <Layout.Header className={"loading-header"}>
            <img src={"./logo3.png"} alt={""}/>
        </Layout.Header>
        <Layout.Content className={"loading-content"}>
            <Spin tip={"Происходит магия поиска...."}/>
        </Layout.Content>
    </React.Fragment>
)