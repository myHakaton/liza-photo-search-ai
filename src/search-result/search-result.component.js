import React from "react";
import {Layout, Image} from "antd";
import {SearchComponent} from "../search/search.component";
import "./search-result.styles.css";
import {useRecoilValue} from "recoil";
import {SelectSearchImageByTagList} from "../api/api.state";

export const SearchResultPage = () => {
    const imageList = useRecoilValue(SelectSearchImageByTagList);
    return (
        <React.Fragment>
            <Layout.Header className={"search-result-header"}>
                <img src={"./logo3.png"} alt={""}/>
                <div className={"search"}>
                    <SearchComponent/>
                </div>
            </Layout.Header>
            <Layout.Content className={"search-result-content"}>
                <Image.PreviewGroup>
                    {imageList.map((item, index) => (
                        <Image
                            key={index}
                            style={{padding: 6}}
                            width={200}
                            src={`http://172.29.29.20:9000/logisticsphoto/${item}`}
                        />
                    ))}
                </Image.PreviewGroup>
            </Layout.Content>
        </React.Fragment>
    )
}